#!/usr/bin/env bash
cd "$(dirname $0)"
./build.sh
cd build
java eu.codefionn.helloworldprinter.HelloWorldPrinter
