package eu.codefionn.helloworldprinter;

class HelloWorldPrinter {
	public void print() {
		System.out.println("Hello, World!");
	}

	public static void helloWorldPrinter() {
		// printer is created in memory
		HelloWorldPrinter printer = new HelloWorldPrinter();
		// Prints "Hello, World!"
		printer.print();
		// printer is not in scope any delete it
	}

	public static void main(String[] args) {
		helloWorldPrinter();
	}
}
