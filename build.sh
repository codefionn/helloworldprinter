#!/usr/bin/env bash
cd "$(dirname $0)"
! [ -f "build" ]  || mkdir build
cd src
javac -d ../build eu/codefionn/helloworldprinter/HelloWorldPrinter.java

